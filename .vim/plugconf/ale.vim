" 左コラムは常に表示
let g:ale_sign_column_always = 1

let g:ale_sign_error = ''
let g:ale_sign_warning = ''

let g:ale_open_list = 1
"let g:ale_keep_list_window_open = 1

" 保存時のみ lint 実行
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'

let g:ale_cpp_cpplint_options = '--fileter=-,+build/class,+build/c++11,+build/deprecated,+build/endif_comment,+build/explicit_make_pair,+build/forward_decl,+build/include_what_you_use,+build/namespaces,+build/printf_format,+build/storage_class,+readability/check,+readability/constructors,+readability/fn_size,+readability/inheritance,+readability/multiline_string,+readability/namespace,+readability/nolint,+readability/strings,+readability/utf8,+runtime/arrays,+runtime/casting,+runtime/explicit,+runtime/indentation_namespace,+runtime/init,+runtime/invalid_increment,+runtime/member_string_references,+runtime/memset,+runtime/operator,+runtime/printf_format,+runtime/string,+runtime/threadsafe_fn,+runtime/vlog,+whitespace/empty_conditional_body,+whitespace/forcolon'

nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

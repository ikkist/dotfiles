" vim-plug

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl --insecure -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'tomasr/molokai'
Plug 'bronson/vim-trailing-whitespace'
Plug 'itchyny/lightline.vim'
Plug 'kannokanno/previm'
Plug 'fatih/vim-go'
"Plug 'w0rp/ale'
Plug 'mileszs/ack.vim' " The silver seacher
Plug 'chazy/cscope_maps'
Plug 'terryma/vim-multiple-cursors'
Plug 'posva/vim-vue'
call plug#end()

" tab = 4space
set tabstop=4
set autoindent
set expandtab
set shiftwidth=4

"対応した括弧を表示
"set showmatch

" デフォルトで行数を表示
set nu

" マウス有効
"set mouse=a

" 256色表示にする
set t_Co=256

" インクリメンタルサーチ有効
set incsearch

" 大文字小文字無視して検索
set ignorecase

"ファイルタイプごとに単語色分け
syntax on

" clipboard を有効化
set clipboard&
set clipboard^=unnamedplus

" backspace 効かない対策
set backspace=indent,eol,start

" backup をとらない
set nobackup

" buffer 移動時に保存しなくてよくなる
set hidden

" color
colorscheme molokai

autocmd QuickFixCmdPost *grep* cwindow
autocmd QuickFixCmdPost *Ag* cwindow

set encoding=utf8
set ambiwidth=double
source ~/.vim/plugconf/lightline.vim
source ~/.vim/plugconf/binary.vim
source ~/.vim/plugconf/charcode.vim
"source ~/.vim/plugconf/ale.vim
source ~/.vim/plugconf/ack.vim
source ~/.vim/plugconf/cscope.vim

" Mouse enable
set mouse=a
set ttymouse=xterm2

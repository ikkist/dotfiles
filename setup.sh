#!/bin/bash

for f in .??*
do
    [[ "$f" == ".git" ]] && continue
    [[ "$f" == ".gitignore" ]] && continue
    echo "$f"
    ln -s $(realpath $f) ~/$f
done
